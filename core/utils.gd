### <EC::GD_CORE_UTILS>
###
### Global class containing multiple utility static functions.
###
class_name Utils



### Sets all of the processes (except internal processes) of the given.
### 
static func set_all_processes(node: Node, enable: bool) -> void:
	node.set_process(enable)
	node.set_process_input(enable)
	node.set_process_shortcut_input(enable)
	node.set_process_unhandled_input(enable)
	node.set_process_unhandled_key_input(enable)
	node.set_physics_process(enable)
