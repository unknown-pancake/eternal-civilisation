### <EC::GD_CORE_FSM_STATE>
###
### Base class for all FSM states.
###
### All [`FsmState`] must be a child of a [`FsmController`] node to work 
### properly. 
###
### See also:
###		[`FsmController`]
###
class_name FsmState
extends Node



### Indicates if the state is volatile or not.
###
### A volatile state can't have another state on top of it in the 
### [`FsmController`]'s state stack. If a state is pushed on top of a volatile
### state, a switch operation will be done instead and thus removing the 
### volatile state.
###
@export var volatile := false



### Controller managing the state.
###
@onready var controller: FsmController = get_parent() as FsmController



### Notifies the state it has been entered.
###
### Called by the [`FsmController`] when the state is pushed to the top of 
### controller's stack.
###
### Parameters : 
### 	_data: any -> Optional data given to the state.
###
func notify_entered(_data) -> void:
	pass

### Notifies the state it has been resumed.
###
### Called by the [`FsmController`] when the state becomes the top state in the
### controller's stack. It is always called after [`notify_entered`].
###
### By default, this method enables all of the state's processes.
###
func notify_resumed() -> void:
	Utils.set_all_processes(self, true)

### Notifies the state it has been paused.
###
### Called by the [`FsmController`] when the state is no longer the top state in
### the controller's stack (including when the state is removed from the stack).
### It is always called before [`notify_exited`].
###
### By default, this method disables all of the state's processes.
###
func notify_paused() -> void:
	Utils.set_all_processes(self, false)

### Notifies the state it has been exited.
###
### Called by the [`FsmController`] when the state is removed from the 
### controller's stack.
###
func notify_exited() -> void:
	pass
