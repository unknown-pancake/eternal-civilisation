### <EC::GD_CORE_FSM_CONTROLLER>
###
### FSM controller, managing multiple [`FsmState`] through a state stack, where
### only the top state is active.
###
### When the controller is ready, all of its children (state or not) have all
### of their processes disabled.
###
### # State life cycle methods #
### ============================
###
### There is 4 life cycle methods for states, each one representing an event
### in the stack relative to the state. 
###
###  1. When a state is pushed on the stack (either through [`push`] or 
###		[`switch`]), their [`notify_entered`] method is called to notify the 
###		state is has been added to the stack. It is also called "entering the 
###		state".
###  2. When a state becomes the state at the top of the stack (either through
###		[`push`], [`pop`], or [`switch`]), their [`notify_resumed`] method is 
###		called to notify the state it is now the active state. It is also called
###		"resuming the state".
###	 3. When a state is no longer at the top of the stack (either through
###		[`push`], [`pop`], or [`switch`]), their [`notify_paused`] method is
###		called to notify the state it isn't the active state anymore. It is also
###		called "pausing the state".
###	 4. When a state is removed from the stack (either through [`pop`] or 
###		[`switch`]), their [`notify_exited`] method is called to notify the 
###		state it is no longer in the stack. It is also called "exiting the 
###		state".
###
### # Stack manipulation methods #
### ==============================
###
### There is 3 availables stack operation :
###
###  1. [`push`] : It pushes a new state to the top of the stack. The previous 
###		state (if the stack isn't empty) is just paused. Then, the added state 
###		is entered then resumed.
###	 2. [`pop`] : It pops the top state of stack. The removed stack is paused
###		then exited. The new top state (if the stack isn't empty) is resumed.
###	 3. [`switch`] : It switches the top state of the stack with another one.
###		The replaced state is paused then exited. The new state is pushed to the 
###		stack before being entered and resumed. This operation only notify the
###		removed state and its replacement without notifying any other states.
###
### [`push`] and [`switch`] reference state by their name. This name is simply
### the node's name of the state, relative to the controller in the tree.
###
class_name FsmController
extends Node



### Pushes the given state (represented by the given state name) on top of the
### stack, passing to it the optional data.
###
### Note:
###		If the state at the top of the stack is volatile, [`switch`] is called
### instead.
###
### Parameters:
###		state: String -> Name of the state to push.
###		data: any -> Optional data to give to the pushed state.
###
### See also:
###		[`FsmController`]
###
func push(state: String, data = null) -> void:
	_call_current_state_method("notify_paused")
	
	_stack.push_back(get_node(state))
	
	_call_current_state_method("notify_entered", [data])
	_call_current_state_method("notify_resumed")

### Pops the state at the top of the stack.
###
### Note:
###		If the controller's state stack is empty, nothing happens.
###
### See also:
###		[`FsmController`]
###
func pop() -> void:
	if _stack.size() == 0:
		return
	
	_call_current_state_method("notify_paused")
	_call_current_state_method("notify_exited")
	
	_stack.pop_back()
	
	_call_current_state_method("notify_resumed")

### Switches the state at the top of the stack with the given state (represented
### by the given state name).
###
### Note:
###		If the controller's state stack is empty, this method is equivalent
### to [`push`].
###
### Parameters:
###		state: String -> Name of the state used as a replacement for the 
###			current top state.
###		data: any -> Optional data to give to the replacement state.
###
### See also:
###		[`FsmController`]
###
func switch(state: String, data = null) -> void:
	if _stack.size() == 0:
		push(state, data)
		return
	
	_call_current_state_method("notify_paused")
	_call_current_state_method("notify_exited")
	
	_stack[_stack.size() - 1] = get_node(state)
	
	_call_current_state_method("notify_entered", [data])
	_call_current_state_method("notify_resumed")



### Gets the current active state, or null if the controller's state stack is
### empty.
###
### Returns:
###		The current active state, or null.
###
func get_current_state() -> FsmState:
	return null if _stack.size() == 0 else _stack.back()



### Controller's state stack.
###
var _stack: Array[FsmState] = []



### Called when all of the children nodes are ready.
###
### Disables all of the processes of all the controller's children (state or 
### not).
###
func _ready() -> void:
	for child in get_children():
		Utils.set_all_processes(child, false)



### Calls the given method on the current active state. If the stack is empty,
### this method does nothing.
###
### Parameters:
###		fn: String -> Name of the method to call.
###		args: Array -> Arguments to pass to the method called.
###
func _call_current_state_method(fn: String, args := []) -> void:
	var state = get_current_state()
	
	if state:
		state.callv(fn, args)
