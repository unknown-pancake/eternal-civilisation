### <EC::GD_GAME_PLAYER_LOGIC_INPUTS>
###
### Manages and handles the player's inputs based on the input's rules, without
### regards to the player's current state.
###
### # Directional inputs #
### ======================
###
### This class gives a shortcut to obtain directional inputs : 
###   - `move-up`, `move-down`, `move-left`, `move-right` : All of these inputs
###		can be accessed through [`move_vector`].
###   - `aux-up`, `aux-down`, `aux-left`, `aux-right` : All of these inputs can
###		be accessed through [`aux_vector`].
###
### # Other inputs #
### ================
### 
### This class also provides tools to query and (when available) manipulate the
### player's inputs.
###
###	  - `jump` : This input can only be pressed. It has a short buffer.
###	  - `dodge` : This input can be held or released. It has a short buffer for
###		the release but if held long enough, the input detected will be 
###		`sprint`.
###	  - `sprint` : Fake input. See `dodge`.
###	  - `light-attack` : This input can only be pressed. It has a short buffer.
###	  - `heavy-attack` : This input can only be pressed. It has a short buffer.
###	  - `interact` : This input can be held or released. It has a short buffer
###		for the release but if held long enough, the input detected will be 
###		`inspect`.
###	  - `inspect` : Fake input. See `interact`.
###	  - `use-item` : This input can only be pressed. It has a short buffer.
###	  - `previous-item` : This input can only be pressed. It has no impact
###		in-game and as such when pressed, the manager only emits the 
###		[`previous_item_pressed`] signal.
###	  - `next-item` : This input can only be pressed. It has no impact in-game
###		and as such when pressed, the manager only emits the 
###		[`next_item_pressed`] signal.
###	  - `boost-jump` : Fake input. It is dependent on the heavy attack animation
###		and as such the manager only emits a signal when the `aux-up` input is
###		pressed.
###	  - `hook` : Fake input. It has a small buffer and it is triggered when
###		`aux-left` or `aux-right` are held and `light-attack` is pressed.
###	  - `dive` : Fake input. It isn't handled by the input manager at it is 
###		dependent on the player's state.
###
class_name PlayerInputManager
extends Node



### Emitted when the `previous-item` input is pressed. The input manager doesn't
### do anything else than emitting this signal for this input.
###
signal previous_item_pressed()

### Emitted when the `next-item` input is pressed. The input manager doesn't
### do anything else than emitting this signal for this input.
###
signal next_item_pressed()

### Emitted when the `aux-up` input is pressed. This is used by for the 
### `boost-jump` fake input which depends on the player's state and thus cannot
### be implemented in the input manager.
###
signal aux_up_pressed()



### Retrieves the vector corresponding to the `move-*` inputs.
###
### Returns:
###		The vector corresponding to the `move-*` inputs.
###
func move_vector() -> Vector2:
	return Input.get_vector(
		"move-left", "move-right", 
		"move-up", "move-down"
	)

### Retrieves the vector corresponding to the aux-*` inputs.
###
### Returns:
###		The vector corresponding to the aux-*` inputs.
###
func aux_vector() -> Vector2:
	return Input.get_vector(
		"aux-left", "aux-right", 
		"aux-up", "aux-down"
	)



### Checks if the jump input is pressed. It takes into account the input's 
### buffering.
###
### Returns:
###		True if the jump input was pressed within the buffer window, false 
### otherwise.
### 
func is_jump_pressed() -> bool:
	return _check_and_stop_timer(_jump_buffer)



### Chceks if the dodge input is released. It takes into account the input's
### buffering.
###
### Note: 
###		The dodge input has multiple usages (notable with sprinting too). If the
### dodge input is held for long enough, the input detected will be `sprint` and
### not dodge.
###
### Returns:
###		True if the dodge input was released within the buffer window, false
### otherwise.
###
func is_dodge_released() -> bool:
	return _check_and_stop_timer(_dodge_buffer)

### Checks if the sprint input is held pressed. This doesn't have any buffering.
###
### Returns:
###		True if the sprint input is held pressed, false otherwise.
###
### See also:
### 	[`is_dodge_released`].
###
func is_sprint_held() -> bool:
	return _sprinting_triggered



### Checks if the light attack input is pressed. This takes into account the
### input's buffering.
###
### Returns:
### 	True if the light attack input was pressed within the buffer window,
### false otherwise.
###
func is_light_attack_pressed() -> bool:
	return _check_and_stop_timer(_light_attack_buffer)



### Checks if the heavy attack input is pressed. This takes into account the
### input's buffering.
###
### Returns:
### 	True if the heavy attack input was pressed within the buffer window,
### false otherwise.
###
func is_heavy_attack_pressed() -> bool:
	return _check_and_stop_timer(_heavy_attack_buffer)



### Chceks if the interact input is released. It takes into account the input's
### buffering.
###
### Note: 
###		The interact input has multiple usages (notable with checking too). If 
### the interact input is held for long enough, the input detected will be 
### `inspect` and not dodge.
###
### Returns:
###		True if the interact input was released within the buffer window, false
### otherwise.
###
func is_interact_released() -> bool:
	return _check_and_stop_timer(_interact_buffer)

### Checks if the inspect input is held pressed. This doesn't have any buffering.
###
### Returns:
###		True if the inspect input is held pressed, false otherwise.
###
### See also:
### 	[`is_dodge_released`].
###
func is_inspect_held() -> bool:
	return _inspecting_triggered



### Checks if the use item input is pressed. This takes into account the
### input's buffering.
###
### Returns:
### 	True if the use item input was pressed within the buffer window, false 
### otherwise.
###
func is_use_item_pressed() -> bool:
	return _check_and_stop_timer(_use_item_buffer)



### Checks if the hook input is pressed. This takes into account the input's 
### buffering.
###
### Returns:
### 	True if the hook input was pressed within the buffer window, false 
### otherwise.
###
func is_hook_pressed() -> bool:
	return _check_and_stop_timer(_hook_buffer)



### Buffer for the `jump` input.
###
@onready var _jump_buffer: Timer = $jump_buffer as Timer

### Buffer for the `dodge` input.
###
@onready var _dodge_buffer: Timer = $dodge_buffer as Timer

### Buffer for the `light-attack` input.
###
@onready var _light_attack_buffer: Timer = $light_attack_buffer as Timer

### Buffer for the `heavy-attack` input.
###
@onready var _heavy_attack_buffer: Timer = $heavy_attack_buffer as Timer

### Buffer for the `interact` input.
###
@onready var _interact_buffer: Timer = $interact_buffer as Timer

### Buffer for the `use-item` input.
###
@onready var _use_item_buffer: Timer = $use_item_buffer as Timer

### Buffer for the `hook` fake input.
###
@onready var _hook_buffer: Timer = $hook_buffer as Timer

### Delay before triggering `sprint` if `dodge` is held pressed long enough.
###
@onready var _sprint_delay: Timer = $sprint_delay as Timer

### Delay before triggering `inspect` if `interact` is held pressed long enough.
###
@onready var _inspect_delay: Timer = $inspect_delay as Timer



### Keeps track of if the `sprint` fake input was triggered before the release 
### of the `dodge` input.
###
var _sprinting_triggered := false

### Keeps track of if the `inspect` fake input was triggered before the release 
### of the `interact` input.
###
var _inspecting_triggered := false



### Checks if the given timer is still running. If it is, the timer is stoped.
###
### Parameters:
###		timer: Timer -> Timer to check
###
### Returns:
###		True if the timer is still running, false otherwise.
###
func _check_and_stop_timer(timer: Timer) -> bool:
	if not timer.is_stopped():
		timer.stop()
		return true
	
	return false



### Called when the `sprint_delay` timer ends.
###
func _on_sprint_delay_timeout() -> void:
	_sprinting_triggered = true

### Called when the `inspect_delay` timer ends.
###
func _on_inspect_delay_timeout() -> void:
	_inspecting_triggered = true



### Handles inputs.
###
func _unhandled_input(ev: InputEvent) -> void:
	# `jump` input handling
	if ev.is_action_pressed("jump"):
		_jump_buffer.start()
		
	# `dodge` and `sprint` input handling
	elif ev.is_action_pressed("dodge"):
		_sprint_delay.start()
	elif ev.is_action_released("dodge"):
		if _sprinting_triggered:
			_sprinting_triggered = false
		else:
			_sprint_delay.stop()
			_dodge_buffer.start()
	
	# `light-attack` and `hook` input handling
	elif ev.is_action_pressed("light-attack"):
		if aux_vector().x != 0:
			_hook_buffer.start()
		else:
			_light_attack_buffer.start()
	
	# `heavy-attack` input handling
	elif ev.is_action_pressed("heavy-attack"):
		_heavy_attack_buffer.start()

	# `interact` and `inspect` input handling
	elif ev.is_action_pressed("interact"):
		_inspect_delay.start()
	elif ev.is_action_released("interact"):
		if _inspecting_triggered:
			_inspecting_triggered = false
		else:
			_inspect_delay.stop()
			_interact_buffer.start()
	
	# `use-item` input handling
	elif ev.is_action_pressed("use-item"):
		_use_item_buffer.start()
	
	# `previous-item` and `next-item` input handling
	elif ev.is_action_pressed("previous-item"):
		previous_item_pressed.emit()
	elif ev.is_action_pressed("next-item"):
		next_item_pressed.emit()
	
	# `boost-jump` input handling
	elif ev.is_action_pressed("aux-up"):
		aux_up_pressed.emit()
