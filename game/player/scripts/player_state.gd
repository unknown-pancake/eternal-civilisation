### <EC::GD_GAME_PLAYER_BASE_STATE>
###
### Base class for all of the player's FSM states.
###
class_name PlayerBaseState
extends FsmState



### References the player controller.
###
@onready var player: PlayerController = owner as PlayerController
